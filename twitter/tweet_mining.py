import json
import pandas as pd
import matplotlib.pyplot as plt
import re
from pprint import pprint
# reference: http://adilmoujahid.com/posts/2014/07/twitter-analytics/
# Modified to work with python 3.1
# Text mining of results from twitter stream containing keywords: python, javascript, ruby

def safe(tweet,key,defaultValue):
    if key in tweet:
        if tweet[key] == None:
            return defaultValue
        return str(tweet[key])
    return defaultValue

def country(tweet,defaultValue):
    key = 'place'
    if key in tweet:
        if tweet[key] == None:
            return defaultValue
        return str(tweet[key]['country'])
    return defaultValue

def user(tweet,defaultValue):
    key = 'user'
    if key in tweet:
        if tweet[key] == None:
            return defaultValue
        if tweet[key]['location'] == None:
            return defaultValue   
        return str(tweet[key]['location'].encode('utf8'))
    return defaultValue

def timezone(tweet,defaultValue):
    key = 'user'
    if key in tweet:
        if tweet[key] == None:
            return defaultValue
        if tweet[key]['time_zone'] == None:
            return defaultValue   
        return tweet[key]['time_zone']
    return defaultValue
        
tweets_data_path = 'twitter_streaming_report.txt'

tweets_data = []
tweets_file = open(tweets_data_path, "r")
for line in tweets_file:
    try:
        tweet = json.loads(line)
        tweets_data.append(tweet)
    except:
        continue

print("#tweets="+str(len(tweets_data)));
for line in tweets_data:
    #print("lang="+line['lang']+",country="+safe(line,'place','')+" ?="+str(line.keys()))
    print("lang="+line['lang']+",place="+country(line,'')+",user.locaton="+user(line,'')+",timezone="+timezone(line,''))

tweets = pd.DataFrame()

tweets['text'] = list(map(lambda tweet: tweet['text'], tweets_data))
tweets['lang'] = list(map(lambda tweet: tweet['lang'], tweets_data))
tweets['country'] = list(map(lambda tweet: tweet['place']['country'] if tweet['place'] != None else None, tweets_data))


tweets_by_lang = tweets.lang.value_counts()
    
fig, ax = plt.subplots()
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=10)
ax.set_xlabel('Languages', fontsize=15)
ax.set_ylabel('Number of tweets' , fontsize=15)
ax.set_title('Top 5 languages', fontsize=15, fontweight='bold')
tweets_by_lang[:5].plot(ax=ax, kind='bar', color='red')
fig.show()

tweets_by_country = tweets.country.value_counts()

fig, ax = plt.subplots()
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=10)
ax.set_xlabel('Countries', fontsize=15)
ax.set_ylabel('Number of tweets' , fontsize=15)
ax.set_title('Top 5 countries', fontsize=15, fontweight='bold')
tweets_by_country[:5].plot(ax=ax, kind='bar', color='blue')
fig.show()

# tweets with keywords
def word_in_text(word, text):
    word = word.lower()
    text = text.lower()
    match = re.search(word, text)
    if match:
        return True
    return False
    
tweets['python'] = tweets['text'].apply(lambda tweet: word_in_text('python', tweet))
tweets['javascript'] = tweets['text'].apply(lambda tweet: word_in_text('javascript', tweet))
tweets['ruby'] = tweets['text'].apply(lambda tweet: word_in_text('ruby', tweet))

print(tweets['python'].value_counts()[True])
print(tweets['javascript'].value_counts()[True])
print(tweets['ruby'].value_counts()[True])

prg_langs = ['python', 'javascript', 'ruby']
tweets_by_prg_lang = [tweets['python'].value_counts()[True], tweets['javascript'].value_counts()[True], tweets['ruby'].value_counts()[True]]

x_pos = list(range(len(prg_langs)))
width = 0.8
fig, ax = plt.subplots()
plt.bar(x_pos, tweets_by_prg_lang, width, alpha=1, color='g')

# Setting axis labels and ticks
ax.set_ylabel('Number of tweets', fontsize=15)
ax.set_title('Ranking: python vs. javascript vs. ruby (Raw data)', fontsize=10, fontweight='bold')
ax.set_xticks([p + 0.4 * width for p in x_pos])
ax.set_xticklabels(prg_langs)
plt.grid()
fig.show()

# Targeting relevant tweets

tweets['programming'] = tweets['text'].apply(lambda tweet: word_in_text('programming', tweet))
tweets['tutorial'] = tweets['text'].apply(lambda tweet: word_in_text('tutorial', tweet))
tweets['relevant'] = tweets['text'].apply(lambda tweet: word_in_text('programming', tweet) or word_in_text('tutorial', tweet))

print(tweets['programming'].value_counts()[True])
print(tweets['tutorial'].value_counts()[True])
print(tweets['relevant'].value_counts()[True])

tweets_by_prg_lang = [tweets[tweets['relevant'] == True]['python'].value_counts()[True], 
                      tweets[tweets['relevant'] == True]['javascript'].value_counts()[True], 
                      tweets[tweets['relevant'] == True]['ruby'].value_counts()[True]]
x_pos = list(range(len(prg_langs)))
width = 0.8
fig, ax = plt.subplots()
plt.bar(x_pos, tweets_by_prg_lang, width,alpha=1,color='g')
ax.set_ylabel('Number of tweets', fontsize=15)
ax.set_title('Ranking: python vs. javascript vs. ruby (Relevant data)', fontsize=10, fontweight='bold')
ax.set_xticks([p + 0.4 * width for p in x_pos])
ax.set_xticklabels(prg_langs)
plt.grid()
fig.show()

# extracting links
def extract_link(text):
    regex = r'https?://[^\s<>"]+|www\.[^\s<>"]+'
    match = re.search(regex, text)
    if match:
        return match.group()
    return ''
    
tweets['link'] = tweets['text'].apply(lambda tweet: extract_link(tweet))
tweets_relevant = tweets[tweets['relevant'] == True]                        # relevant
tweets_relevant_with_link = tweets_relevant[tweets_relevant['link'] != '']  # no blank

print(tweets_relevant_with_link[tweets_relevant_with_link['python'] == True]['link'])
print(tweets_relevant_with_link[tweets_relevant_with_link['javascript'] == True]['link'])
print(tweets_relevant_with_link[tweets_relevant_with_link['ruby'] == True]['link'])

input("Press Enter to continue...")
